﻿using System;
using System.Globalization;

namespace NumeralSystems
{
    public static class Converter
    {
        public static string GetPositiveOctal(this int number)
        {
            if (number < 0)
            {
                throw new ArgumentException("Number is less than zero.");
            }

            return GetRadix(number, 8);
        }

        public static string GetPositiveDecimal(this int number)
        {
            if (number < 0)
            {
                throw new ArgumentException("Number is less than zero.");
            }

            return GetRadix(number, 10);
        }

        public static string GetPositiveHex(this int number)
        {
            if (number < 0)
            {
                throw new ArgumentException("Number is less than zero.");
            }

            return GetRadix(number, 16);
        }

        public static string GetPositiveRadix(this int number, int radix)
        {
            if (number < 0)
            {
                throw new ArgumentException("Number is less than zero.");
            }

            if (radix != 8 && radix != 10 && radix != 16)
            {
                throw new ArgumentException("Radix has the wrong value.");
            }

            if (radix == 8)
            {
                return GetPositiveOctal(number);
            }

            if (radix == 10)
            {
                return GetPositiveDecimal(number);
            }

            return GetPositiveHex(number);
        }

        public static string GetRadix(this int number, int radix)
        {
            if (radix != 8 && radix != 10 && radix != 16)
            {
                throw new ArgumentException("Radix has the wrong value.");
            }

            string binaryNumber = string.Empty;
            int copyNumber = number;

            while (copyNumber != 0)
            {
                binaryNumber = string.Concat(copyNumber % 2 == 0 ? '0' : '1', binaryNumber);
                copyNumber /= 2;
            }

            if (number < 0)
            {
                binaryNumber = ConvertBinaryNumber(binaryNumber);
            }

            switch (radix)
            {
                case 8:
                    return ConvertToOctal(binaryNumber);
                case 16:
                    return ConvertToHex(binaryNumber);
            }

            return number.ToString(CultureInfo.CurrentCulture);
        }

        public static string ConvertBinaryNumber(string binaryNumber)
        {
            if (binaryNumber is null)
            {
                throw new ArgumentNullException(nameof(binaryNumber));
            }

            char[] arrayBinary = binaryNumber.ToCharArray();
            for (int i = 0; i < arrayBinary.Length; i++)
            {
                if (arrayBinary[i] == '0')
                {
                    arrayBinary[i] = '1';
                    continue;
                }

                arrayBinary[i] = '0';
            }

            for (int i = arrayBinary.Length - 1; i >= 0; i--)
            {
                if (arrayBinary[i] == '1')
                {
                    arrayBinary[i] = '0';
                    continue;
                }

                arrayBinary[i] = '1';
                break;
            }

            return string.Concat(new string('1', (sizeof(int) * 8) - arrayBinary.Length), new string(arrayBinary));
        }

        public static string ConvertToOctal(string binaryNumber)
        {
            if (binaryNumber == null)
            {
                throw new ArgumentNullException(nameof(binaryNumber));
            }

            string octalNumber = string.Empty;
            char[] octalArray = new char[] { '0', '1', '2', '3', '4', '5', '6', '7' };
            string[] binaryArray = new string[] { "000", "001", "010", "011", "100", "101", "110", "111" };

            if (binaryNumber.Length % 3 != 0)
            {
                binaryNumber = string.Concat(new string('0', 3 - (binaryNumber.Length % 3)), binaryNumber);
            }

            for (int i = 3; i <= binaryNumber.Length; i += 3)
            {
                for (int j = 0; j < binaryArray.Length; j++)
                {
                    if (binaryNumber[(i - 3) .. i] == binaryArray[j])
                    {
                        octalNumber = string.Concat(octalNumber, octalArray[j]);
                        break;
                    }
                }
            }

            return octalNumber;
        }

        public static string ConvertToHex(string binaryNumber)
        {
            if (binaryNumber == null)
            {
                throw new ArgumentNullException(nameof(binaryNumber));
            }

            string hexNumber = string.Empty;
            char[] hexArray = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
            string[] binaryArray = new string[] { "0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111" };

            if (binaryNumber.Length % 4 != 0)
            {
                binaryNumber = string.Concat(new string('0', 4 - (binaryNumber.Length % 4)), binaryNumber);
            }

            for (int i = 4; i <= binaryNumber.Length; i += 4)
            {
                for (int j = 0; j < binaryArray.Length; j++)
                {
                    if (binaryNumber[(i - 4) .. i] == binaryArray[j])
                    {
                        hexNumber = string.Concat(hexNumber, hexArray[j]);
                        break;
                    }
                }
            }

            return hexNumber;
        }
    }
}
